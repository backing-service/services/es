PROJECT_LIST_SERVICES += es

ES_COMPOSE_FILE ?= $(RESOURCES)/es/docker-compose
COMPOSE_FILE += -f $(ES_COMPOSE_FILE).yml -f $(ES_COMPOSE_FILE).$(ENV).yml

PRE_TARGETS_ENV += es-env
TARGETS_HELP += es-help
PRE_TARGETS_UP+=es-up
PRE_TARGETS_START+=es-start
PRE_TARGETS_STOP+=es-stop
PRE_TARGETS_DOWN+=es-down

#ES_HOST ?= redis
#ES_BASEs ?= 0
#ES_PASS ?= example
#ES_MAX_CLIENTS ?= 10000
#ES_MAX_MEMORY ?= 0

es-help:
	$(call SHOW_TITLE_HELP, ES)
	$(call SHOW_CMD_HELP, es-up) Запускаем окружение UP \(ES\)
	$(call SHOW_CMD_HELP, es-start) Запускаем/стартуем остановленное приложение \(ES\)
	$(call SHOW_CMD_HELP, es-stop) Остановлеваем приложение \(ES\)
	$(call SHOW_CMD_HELP, es-rm) удаляем остановленное приложение \(ES\)

es-up:
	@docker-compose -f $(COMPOSE_FILE) up -d es

es-start:
	@docker-compose -f $(COMPOSE_FILE) start es

es-stop:
	@docker-compose -f $(COMPOSE_FILE) stop es

es-rm:
	@docker-compose -f $(COMPOSE_FILE) rm -s es

es-down: es-stop es-rm

es-cli:
	@docker-compose -f $(COMPOSE_FILE) exec es /bin/bash

es-log:
	@docker logs es

es-env:
	@echo "# ---- ES ----"  >> .env
	@echo "" >> .env
